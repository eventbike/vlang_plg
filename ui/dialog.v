import ui

struct App {
mut:
	window &ui.Window = 0
	text string
}

fn main () {
	mut app := &App{text: "Hello there"}
	app.window = ui.window({
		width: 600
		height: 200
		title: "Test"
		state: app
	}, [
		ui.row({
			spacing: 15
			stretch: false
			margin: ui.MarginConfig{5, 5, 5, 5}
		}, [
			ui.textbox(
				width: 600
				max_len: 100
				read_only: true
				text: &app.text
			),
		]),
		ui.row({
			alignment: .bottom
			spacing: 5
			stretch: false
			margin: ui.MarginConfig{5, 5, 5, 5}
		},[
		ui.column({
			width:600
			spacing: 15
			}, [
				ui.row({
				},[
					ui.button(
						height: 24
						text: 'Button'
						onclick: quit
					),
					ui.button(
						height: 24
						text: 'Another Button'
						// onclick: exit(1)
					)
				])
			])
		])
	])
	ui.run(app.window)
}

fn quit(mut app App, btn &ui.Button) {
	exit(1)
}
