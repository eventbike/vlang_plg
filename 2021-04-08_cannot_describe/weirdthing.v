module main

import vweb

pub struct Project {
	vweb.Context
}

fn main() {
	vweb.run<Project>(9080)
}

struct ExampleStruct {
	example int
}

pub fn (mut app Project) request_raw_2() vweb.Result {
	stuff := []ExampleStruct{}
	return app.request_raw(3)
}

['/request/raw/:foo']
pub fn (mut app Project) request_raw(foo int) vweb.Result {
	return $vweb.html()
}
