module main

struct StructA {
	x int
	y int
}

struct StructB {
	StructA
mut:
	z string
}

fn main() {
	mut b := StructB{
		StructA: StructA{
			x: 1
			y: 2
		}
	}
	b.z = 'hey'
}
