import json
import time

struct Clients {
mut:
	id          int
	key         string
	name        string
	mode        int
	last_active int
}

fn main() {
	mut timer := time.Stopwatch{}
	timer.start()
	sent_client := json.decode(Clients, '') or {
		Clients{
			name: 'Some client who does not know much about valid JSON.'
		}
	}
	println(timer.elapsed())
}
