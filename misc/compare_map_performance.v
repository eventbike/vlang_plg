struct ConfigKeys {
	key0 string
	key1 string
	key2 string
	key3 string
	key4 string
	key5 string
	key6 string
	key7 string
	key8 string
	key9 string
}

fn (config ConfigKeys) print_struct_keys () {
	$for field in ConfigKeys.fields {
		println(config.$(field.name))
	}
}

fn main () {
	mut config := ConfigKeys {}
	$for field in ConfigKeys.fields {
			config.$(field.name) = "abcd"
	}
	print_struct_keys()
}
