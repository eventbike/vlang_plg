import sqlite

struct Someone {
	id        int
	name      string
	shortname string
}

fn main() {
	db := sqlite.connect(':memory:') or { panic(err) }
	db.exec('CREATE TABLE IF NOT EXISTS `someone` (
		`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
		`name`	TEXT,
		`shortname`	TEXT
	);')
	db.exec("INSERT INTO `someone` (id,name,shortname) VALUES (1,'name',NULL)")
	team2 := sql db {
		select from Someone where id == 1
	}
}
