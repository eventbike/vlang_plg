fn thing () {
	println("a")
	println("b")
	println("c")
}

fn main () {
	go thing()
	go fn() {
		println("e")
		println("f")
		println("g")
	}
	println("d")
	println("h")

}
