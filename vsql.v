import lydiandy.vsql.database.sqilte
import lydiandy.vsql

fn main() {
	config := vsql.Config{
		client: 'sqlite'
		host: 'localhost'
		port: 5432
		user: 'postgres' // change to your user
		password: '' // chagne to your password
		database: 'test.db' // change to your database
	}
	// connect to database with config
	mut db := vsql.connect(config) or { panic('connect error:$err') }
}
