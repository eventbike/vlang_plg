module main

import vweb

const (
	port = 9080
)

struct Vturn {
	vweb.Context
}

fn main() {
	run()
}

fn run() {
	vweb.run<Vturn>(port)
}

pub fn (mut app Vturn) init_once() {
}

pub fn (mut app Vturn) init() {
}

[post]
['/test']
pub fn (mut app Vturn) test() vweb.Result {
	// lets make sure it is not our json return ... no
	return app.text('hey')
	// return app.json("")
}
