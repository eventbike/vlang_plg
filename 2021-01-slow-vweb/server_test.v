module main

import time
import net.http

fn testsuite_begin() {
	go run()
	time.sleep(1)
}

fn test_speed_empty() {
	mut stopwatch := time.StopWatch{}
	stopwatch.start()
	http.post('http://localhost:9080/test', '') or { http.Response{} }
	assert stopwatch.elapsed().milliseconds() < 200
}

fn test_speed_body() {
	mut stopwatch := time.StopWatch{}
	stopwatch.start()
	http.post('http://localhost:9080/test', 'body') or { http.Response{} }
	// that one fails with >~1000 milliseconds
	assert stopwatch.elapsed().milliseconds() < 200
}
