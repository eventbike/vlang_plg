module main

import vweb


const (
	port = 9080
)

pub struct App {
	vweb.Context
}

fn main() {
	vweb.run<App>(port)
}

pub fn (mut app App) index() vweb.Result {
	return $vweb.html()
}
