module main

import sqlite
import os

fn main() {
	name := os.input('Please enter the name of the DB: ')
	os.create(name + '.db')
	mut db := sqlite.connect(name + '.db') or { panic(err) }
	file := read_lines('test.sql') or { panic(err) }
	for line in file {
		db.exec(line)
	}
	db.close()
	println('DB $name was created and a test user was inserted.')
}
